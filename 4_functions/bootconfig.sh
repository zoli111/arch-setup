#! /bin/bash

create_efistub() {
	PARAMS="$*"

	LABEL='Arch'
	KERNEL='linux'

	BOOTDISK=$(grep '/boot' /proc/mounts | cut -d' ' -f1 | grep -o '.*[^0-9]')
	BOOTPART=$(grep '/boot' /proc/mounts | cut -d' ' -f1 | grep -Eo '[0-9]+' | tail -1)

	ROOT="UUID=$(sudo blkid --match-tag UUID --output value "$(grep ' / ' /proc/mounts | cut -d' ' -f1)")"
	ROOTFSTYPE=$(grep ' / ' /proc/mounts | cut -d' ' -f3)

	sudo efibootmgr --create --disk "$BOOTDISK" --part "$BOOTPART" --label "$LABEL" --loader /vmlinuz-"$KERNEL" --unicode "root=$ROOT rw initrd=\initramfs-$KERNEL.img rootfstype=$ROOTFSTYPE $PARAMS"
}

delete_old_efistub() {
	MATCH='Arch'
	ENTRY_NUMS=$(efibootmgr | grep -i "$MATCH" | cut -c5-8)
	if [[ -n "$ENTRY_NUMS" ]]; then
		# It's possible to have more entry numbers
		for ENTRY_NUM in "${ENTRY_NUMS[@]}"; do
			sudo efibootmgr --delete-bootnum --bootnum "$ENTRY_NUM" --unicode
		done
	fi
}
