#! /bin/bash

# If the device has a battery we treat it as a laptop
is_laptop() {
	if [[ -d /sys/class/power_supply/BAT0 ]]; then
		return 0
	fi
	return 1
}

# Get videocard manufacturer
vga() {
	if [[ $(lspci | grep VGA) = *NVIDIA* ]]; then
		echo nvidia
	elif [[ $(lspci | grep VGA) = *Intel* ]]; then
		echo intel
	elif [[ $(lspci | grep VGA) = *AMD* ]]; then
		echo amd
	fi
}

# Get CPU architecture
cpuarch() {
	uname -m
}

# Create unique system IDs based on bios ID
system_id_hash() {
	ID_PATH="$(dirname "$(realpath "$0")")/.machine-id"
	if [[ $(uname -m) == "x86_64" ]]; then
		if [[ -f "$ID_PATH" ]]; then
			cat "$ID_PATH"
		else
			sudo sha256sum /sys/class/dmi/id/product_uuid | cut -d' ' -f1 | tee "$ID_PATH"
		fi
	else
		return 1
	fi
}

export my_laptop_id_hash='557750161fded3171b7e264c8f027b96d36fbf92c9a7e1136c9f4194dad44ce3'
export my_pc_id_hash='ae9128b2e9ddbf32e38229d45df508ebffcac91d4283ebb7d9ea7ee440bc7906'
