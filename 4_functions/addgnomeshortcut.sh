#!/bin/bash

# Usage `addgnomeshortcut <ID> <NAME> <BINDING> <COMMAND>`
# E.g. `addgnomeshortcut command0 'Launch Firefox' '<Super>f' 'firefox'`
addgnomeshortcut() {
	GNOME_CUSTOM_SHORTCUT_LIST=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
	if [[ $GNOME_CUSTOM_SHORTCUT_LIST = '@as []' || $(echo "$GNOME_CUSTOM_SHORTCUT_LIST" | grep "'/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/$1/'") != "" ]]; then
		gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/$1/']"
	else
		gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "${GNOME_CUSTOM_SHORTCUT_LIST//]/,\'/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/$1/\']}"
	fi
	gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/"$1"/ name "$2"
	gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/"$1"/ binding "$3"
	gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/"$1"/ command "$4"
}
