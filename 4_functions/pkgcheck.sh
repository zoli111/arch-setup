#! /bin/bash

# Check if package exists
pkgexists() {
	pacman -Q "$1" > /dev/null 2>&1
}
