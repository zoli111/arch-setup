#! /bin/bash

# Sudo loop in the background to not lose root access
# The default sudo timeout is 5 minutes
# Just source this scriptlet

while true; do
	sudo --validate
	sleep 295
done &

SUDO_KEEP_PID=$!

trap 'kill "$SUDO_KEEP_PID"' EXIT
