#! /bin/bash

# Functions to hide or rename .desktop files from menus
hideicon() {
	if [[ -f /usr/share/applications/$1 ]]; then
		sed 's/\[Desktop Entry\]/&\nHidden=true/' /usr/share/applications/"$1" > "$HOME/.local/share/applications/$1"
	fi
}
hideingnome() {
	if [[ -f /usr/share/applications/$1 ]]; then
		sed 's/\[Desktop Entry\]/&\nNotShowIn=GNOME/' /usr/share/applications/"$1" > "$HOME/.local/share/applications/$1"
	fi
}
renameicon() {
	if [[ -f /usr/share/applications/$1 ]]; then
		sed "s/$2/$3/" /usr/share/applications/"$1" > "$HOME/.local/share/applications/$1"
	fi
}
