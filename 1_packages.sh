#! /bin/bash

# Get file directory
SCRIPT_DIR=$(dirname "$(realpath "$0")")

source "$SCRIPT_DIR"/4_functions/sudoaccess.sh
source "$SCRIPT_DIR"/4_functions/hwinfo.sh

# Add permissions to my user
sudo usermod -aG power,video,audio,input,optical,storage "$USER"

# Get fastest pacman mirrors in the current country by ip address
if [[ $(cpuarch) = x86_64 ]]; then
	if ! grep reflector /etc/pacman.d/mirrorlist > /dev/null; then
		sudo pacman -S --needed --noconfirm curl reflector
		COUNTRY_CODE=$(curl -s ipinfo.io | grep -oP '(?<=country": ")[A-Z]+' | tr '[:upper:]' '[:lower:]')
		sudo reflector --country "$COUNTRY_CODE" --latest 5 --delay 1 --fastest 5 --sort rate --save /etc/pacman.d/mirrorlist
	fi
fi

# Uncomment some pacman settings
sudo sed -i 's/#Color/Color/' /etc/pacman.conf
sudo sed -i 's/#ParallelDownloads = [0-9]*/ParallelDownloads = 5/' /etc/pacman.conf

# Install basic system packages
sudo pacman -Sy
sudo pacman -S --needed --noconfirm base-devel bash-completion polkit xdg-user-dirs pacutils pacman-contrib usbutils pciutils git man-db nano vim dosfstools fastfetch tmux

declare -a PACKAGES
declare -a AUR

# Install plymouth
if [[ $(cpuarch) = x86_64 ]]; then
	PACKAGES+=("plymouth")
fi

# Use all threads to build packages
NUMBEROFTHREADS=$(grep -c ^processor /proc/cpuinfo)
sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j'$((NUMBEROFTHREADS + 1))'"/g' /etc/makepkg.conf
sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T '"$NUMBEROFTHREADS"' -z -)/g' /etc/makepkg.conf

# Install paru (if on x86_64 use the prepuilt binary)
if ! pacman -Q paru | grep paru > /dev/null; then
	if [[ $(cpuarch) = x86_64 ]]; then
		git clone https://aur.archlinux.org/paru-bin.git
		cd paru-bin || exit
		makepkg -si --noconfirm
		cd ..
		sudo rm -rf ./paru-bin
	elif [[ $(cpuarch) = aarch64 ]]; then
		sudo pacman -S --needed --noconfirm rust clang
		git clone https://aur.archlinux.org/paru.git
		cd paru || exit
		makepkg -si --noconfirm
		cd ..
		sudo rm -rf ./paru
		sudo pacman -Rns --noconfirm rust clang
	fi
fi
mkdir -p "$HOME"/.config/paru
echo "[options]
PgpFetch
Devel
Provides
DevelSuffixes = -git -cvs -svn -bzr -darcs -always -hg
RemoveMake
SudoLoop
CombinedUpgrade
CleanAfter
NewsOnUpgrade" > "$HOME"/.config/paru/paru.conf

if [[ $(system_id_hash) = "$my_pc_id_hash" ]]; then
	GAMING='1'
fi

# Install videocard drivers
if [[ $(vga) = nvidia ]]; then
	PACKAGES+=("nvidia nvidia-utils" "opencl-nvidia")
	AUR+=("libva-nvidia-driver" "nvidia-tweaks")
	if [[ $GAMING = 1 ]]; then
		PACKAGES+=("lib32-nvidia-utils" "lib32-opencl-nvidia")
	fi
fi
if [[ $(vga) = intel ]]; then
	PACKAGES+=("intel-media-driver" "libvdpau-va-gl" "vulkan-intel" "intel-compute-runtime")
	if [[ $GAMING = 1 ]]; then
		PACKAGES+=("lib32-vulkan-intel")
	fi
fi
if [[ $(vga) = amd ]]; then
	PACKAGES+=("vulkan-radeon" "libva-mesa-driver" "mesa-vdpau")
	if [[ $GAMING = 1 ]]; then
		PACKAGES+=("lib32-vulkan-radeon" "lib32-libva-mesa-driver" "lib32-mesa-vdpau")
	fi
fi

# Install fingerprint sensor driver if needed
if lsusb | grep -i "fingerprint" > /dev/null; then
	PACKAGES+=("fprintd")
fi

if lsusb | grep -i "bluetooth" > /dev/null; then
	PACKAGES+=("bluez")
fi

if [[ $(ls -A /sys/class/backlight/) ]]; then
	PACKAGES+=("acpilight")
fi

if [[ $(cpuarch) == x86_64 && ($(system_id_hash) == "$my_pc_id_hash" || $(system_id_hash) == "$my_laptop_id_hash") ]]; then
	# Install themes
	PACKAGES+=("papirus-icon-theme" "adw-gtk-theme")

	# Install fonts
	PACKAGES+=("inter-font" "adobe-source-code-pro-fonts" "gnu-free-fonts" "ttf-dejavu" "ttf-liberation")

	AUR+=("ttf-twemoji")

	# Install Qt packages
	PACKAGES+=("qt6ct" "qt6-wayland")

	# Install printer support
	PACKAGES+=("cups" "gutenprint" "ghostscript")

	# Install GNOME desktop
	PACKAGES+=("gdm" "polkit-gnome" "gnome-shell" "gnome-control-center" "mutter" "nautilus" "gvfs" "gvfs-mtp" "gvfs-goa" "gnome-console" "file-roller"
		"gnome-text-editor" "loupe" "gnome-calculator" "gnome-disk-utility" "baobab" "gnome-system-monitor" "gnome-logs"
		"gnome-font-viewer" "gnome-menus"
		"snapshot" "power-profiles-daemon" "xdg-desktop-portal" "xdg-desktop-portal-gnome" "dnsmasq" "simple-scan")

	# Install thumbnailers
	PACKAGES+=("gnome-epub-thumbnailer" "ffmpegthumbnailer" "webp-pixbuf-loader")
	AUR+=("simple-thumbnailer-pdf")

	# Install user applications
	PACKAGES+=("firefox" "firefox-i18n-hu" "libreoffice-fresh" "libreoffice-fresh-hu"
		"hunspell" "hunspell-hu" "zed" "inkscape" "gimp" "meld" "rnote" "cmake" "celluloid" "transmission-gtk")

	# Install command line tools
	PACKAGES+=("shfmt" "shellharden")

	# Install some packages only on my desktop pc for gaming
	if [[ $GAMING = 1 ]]; then
		# Gaming packages
		PACKAGES+=("steam-native-runtime" "gamemode" "gnome-boxes")

		# WM components and apps
		PACKAGES+=("sway" "swaybg" "swayidle" "mako" "xdg-desktop-portal-wlr" "waybar" "slurp" "grim" "wl-clipboard" "playerctl" "ttf-font-awesome" "jq"
			"alacritty" "nm-connection-editor" "pavucontrol" "system-config-printer" "blueman")
		AUR+=("rofi-lbonn-wayland-only-git" "swaylock-effects")
	fi
fi

if [[ ${#PACKAGES[*]} ]]; then
	sudo pacman -S --needed --noconfirm "${PACKAGES[@]}"
fi
for package in "${AUR[@]}"; do
	pacman -Q "$package" 2> /dev/null | grep "$package" > /dev/null || paru -S --noconfirm "$package"
done
