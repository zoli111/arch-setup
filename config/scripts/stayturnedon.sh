#!/bin/bash

if [[ $(gsettings get org.gnome.desktop.session idle-delay) = 'uint32 300' ]]; then
	gsettings set org.gnome.desktop.session idle-delay 'uint32 0'
	gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'
	gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type 'nothing'
	notify-send --transient --app-name="Képernyő" --icon="unlock" "A képernyő nem fog kikapcsolni automatikusan"
else
	gsettings reset org.gnome.desktop.session idle-delay
	gsettings reset org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type
	gsettings reset org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type
	notify-send --transient --app-name="Képernyő" --icon="lock" "Ismét automatikusan kikapcsolhat a képernyő"
fi
