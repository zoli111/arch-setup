#!/bin/bash

#gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'

if [[ $(gsettings get org.gnome.desktop.interface color-scheme) = "'prefer-dark'" ]]; then
	# Switch to light theme
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-light'
	gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3'
	gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Light'
	sed -i '/prop oor:name="SymbolStyle"/ s|<value>.*</value>|<value>sifr</value>|' "$HOME"/.config/libreoffice/4/user/registrymodifications.xcu
	sed -i 's/custom_palette=true/custom_palette=false/' "$HOME"/.config/qt6ct/qt6ct.conf
	sed -i 's/icon_theme=Papirus-Dark/icon_theme=Papirus-Light/' "$HOME"/.config/qt6ct/qt6ct.conf
else
	# Switch to dark theme
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
	gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3-dark'
	gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
	sed -i '/prop oor:name="SymbolStyle"/ s|<value>.*</value>|<value>sifr_dark</value>|' "$HOME"/.config/libreoffice/4/user/registrymodifications.xcu
	sed -i 's/custom_palette=false/custom_palette=true/' "$HOME"/.config/qt6ct/qt6ct.conf
	sed -i 's/icon_theme=Papirus-Light/icon_theme=Papirus-Dark/' "$HOME"/.config/qt6ct/qt6ct.conf
fi
