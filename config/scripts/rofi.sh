#!/bin/bash

ROFI_LOGOUT_THEME="
* {
	font: 'Font Awesome 6 Free Solid 13';
}
window {
	width: 600px;
	padding: 0;
}
mainbox {children: [listview];
	horizontal-align: 0.5;
}
listview {
	layout: inherit;
	flow: horizontal;
	columns: 4;
	lines: 1;
	border: 0;
}
element-text {
	horizontal-align: 0.5;
}
element-icon {
	margin: 0;
	size: 0;
	padding: 0;
}
"

NAME_LOGOUT=
NAME_SUSPEND=
NAME_POWEROFF=
NAME_REBOOT=

if [ "$1" = "--logout" ]; then
	command=$(echo -e "$NAME_LOGOUT\n$NAME_SUSPEND\n$NAME_POWEROFF\n$NAME_REBOOT" | rofi -dmenu -theme-str "$ROFI_LOGOUT_THEME")
	case $command in
		"$NAME_LOGOUT")
			swaymsg exit
			;;
		"$NAME_SUSPEND")
			"$HOME"/.config/scripts/lock.sh &
			systemctl suspend
			;;
		"$NAME_POWEROFF")
			poweroff
			;;
		"$NAME_REBOOT")
			reboot
			;;
	esac
else
	rofi -show drun -drun-display-format '{name}'
fi
