#!/bin/bash

if ! updates_arch=$(checkupdates 2> /dev/null | wc -l); then
	updates_arch=0
fi

if ! updates_aur=$(paru -Qum 2> /dev/null | wc -l); then
	updates_aur=0
fi
updates_all=$((updates_arch + updates_aur))

if [[ $updates_all -ge 10 ]]; then
	notify-send "Új csomagfrissítések érhetők el" "$updates_all csomag frissíthető" --icon=software-update-available --app-name=Frissítés
fi
