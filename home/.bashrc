[[ $- != *i* ]] && return

# Aliases
alias ls='ls --color=auto'
alias u='paru'
alias c='clear'
alias decomment='grep -E -v "^[[:space:]]*((#|;|//).*)?$" '

# Envvars
[[ -n "$DISPLAY" ]] && PS1='\[\e[7m\] \u \[\e[m\]\[\e[7m\] \h \[\e[m\]\[\e[7m\] \w \[\e[m\] '

PATH=$PATH:$HOME/.local/bin
