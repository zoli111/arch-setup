#! /bin/bash

# Get file directory
SCRIPT_DIR=$(dirname "$(realpath "$0")")

source "$SCRIPT_DIR"/4_functions/sudoaccess.sh
source "$SCRIPT_DIR"/4_functions/hwinfo.sh
source "$SCRIPT_DIR"/4_functions/pkgcheck.sh
source "$SCRIPT_DIR"/4_functions/bootconfig.sh

# Copy efi file
sudo -u root mkdir -p /boot/efi/boot/
sudo cp /usr/lib/systemd/boot/efi/systemd-bootx64.efi /boot/efi/boot/bootx64.efi

# Kernel parameters
BOOTPARAMS=()
# Plymouth
if pkgexists plymouth; then
	BOOTPARAMS+=(quiet splash)
fi
# Extra performance at the price of security
BOOTPARAMS+=('mitigations=off' 'loglevel=0' 'nowatchdog')

delete_old_efistub
create_efistub "${BOOTPARAMS[@]}"

# Create swapfile
[[ -f /swapfile ]] || sudo mkswap -U clear --size 4G --file /swapfile
grep "swapfile" /etc/fstab > /dev/null || echo "/swapfile none swap defaults 0 0" | sudo tee -a /etc/fstab > /dev/null
echo "vm.swappiness = 20" | sudo tee /etc/sysctl.d/99-swappiness.conf > /dev/null

# Enable plymouth in initcpio
if pkgexists plymouth; then
	grep "plymouth" /etc/mkinitcpio.conf > /dev/null ||
		sudo sed -i '/HOOKS=/s/base/base plymouth/' /etc/mkinitcpio.conf
fi

# Add CPU microcode to initcpio
grep "microcode" /etc/mkinitcpio.conf > /dev/null || sudo sed -i '/HOOKS=/s/autodetect/autodetect microcode/' /etc/mkinitcpio.conf

# Enable nvidia drm and suspend
if [[ $(vga) = nvidia ]]; then
	echo "options nvidia_drm modeset=1" | sudo tee /etc/modprobe.d/nvidia.conf > /dev/null
	echo "options nvidia NVreg_PreserveVideoMemoryAllocations=1" | sudo tee /etc/modprobe.d/nvidia-power.conf > /dev/null
	sudo systemctl enable nvidia-powerd.service
	sudo systemctl enable nvidia-suspend.service
	sudo systemctl enable nvidia-resume.service
fi

if [[ $(vga) = intel ]]; then
	grep "i915" /etc/mkinitcpio.conf > /dev/null ||
		sudo sed -i '/MODULES=/s/)/ i915)/' /etc/mkinitcpio.conf
	sudo sed -i 's/( /(/g' /etc/mkinitcpio.conf
fi

# Enable bluetooth
lsusb | grep -i bluetooth > /dev/null && sudo systemctl enable bluetooth.service
if pkgexists bluez; then
	# Don't start bluetooth automatically
	sudo sed -i -e 's/#AutoEnable=/AutoEnable=/' -e 's/AutoEnable=true/AutoEnable=false/' /etc/bluetooth/main.conf
fi

# Enable & configure GDM
if pkgexists gdm; then
	sudo systemctl enable gdm.service
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
	sudo -u gdm dbus-launch gsettings set org.gnome.login-screen logo ''
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.sound event-sounds false
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.sound input-feedback-sounds false
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.a11y always-show-universal-access-status false
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.interface font-hinting 'full'
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.interface font-antialiasing 'grayscale'
	sudo -u gdm dbus-launch gsettings set org.gnome.desktop.interface font-name 'Inter 11'
	if [[ $(system_id_hash) = "$my_pc_id_hash" ]]; then
		sudo -u gdm dbus-launch gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'
	fi
	[[ -f ~/.config/monitors.xml ]] && sudo cp ~/.config/monitors.xml /var/lib/gdm/.config/monitors.xml
fi

# Enable cups
sudo systemctl enable cups.service

# Enable power-management
if pkgexists tlp; then
	sudo systemctl enable --now tlp
fi

# Set keyboard layout
sudo localectl set-x11-keymap us
sudo localectl set-keymap us

# Set emoji font
if pkgexists ttf-twemoji; then
	sudo ln -sf /usr/share/fontconfig/conf.avail/75-twemoji.conf /etc/fonts/conf.d/75-twemoji.conf
fi

if [[ $(system_id_hash) = "$my_pc_id_hash" ]]; then
	echo "vm.max_map_count=2147483642" | sudo tee /etc/sysctl.d/10-increase-mmap.conf > /dev/null
fi

# It should be in user.sh but it needs sudo access
# Set profile picture for the user (only if it uses my name)
if pkgexists accountsservice; then
	if [[ $USER = zoli && ($(system_id_hash) = "$my_laptop_id_hash" || $(system_id_hash) = "$my_pc_id_hash") ]]; then
		echo "[User]
Session=gnome
Icon=/var/lib/AccountsService/icons/zoli
SystemAccount=false" | sudo tee /var/lib/AccountsService/users/zoli > /dev/null
		curl https://i.imgur.com/l4oHy0z.png --output "$SCRIPT_DIR"/z.png
		sudo mv "$SCRIPT_DIR"/z.png /var/lib/AccountsService/icons/zoli
	fi
fi

sudo mkinitcpio -P
