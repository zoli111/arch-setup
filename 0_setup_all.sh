#! /bin/bash

# Get file directory
SCRIPT_DIR=$(dirname "$(realpath "$0")")

source "$SCRIPT_DIR"/1_packages.sh
source "$SCRIPT_DIR"/2_system.sh
source "$SCRIPT_DIR"/3_user.sh
