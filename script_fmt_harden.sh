#! /bin/bash
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Fix formatting
if command -v shfmt > /dev/null 2>&1; then
	while read -r -d $'\n' SCRIPT; do
		shfmt -w -sr -ci "$SCRIPT"
	done < <(find "$SCRIPT_DIR" -name "*.sh")
else
	echo "shfmt is not available"
fi

# Find possible bugs and errors
if command -v shellharden > /dev/null 2>&1; then
	while read -r -d $'\n' SCRIPT; do
		shellharden --replace "$SCRIPT"
	done < <(find "$SCRIPT_DIR" -name "*.sh")
else
	echo "shellharden is not available"
fi

if command -v shellcheck > /dev/null 2>&1; then
	while read -r -d $'\n' SCRIPT; do
		shellcheck --external-sources "$SCRIPT"
	done < <(find "$SCRIPT_DIR" -name "*.sh")
else
	echo "shellcheck is not available"
fi
