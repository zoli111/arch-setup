#! /bin/bash

# Get file directory
SCRIPT_DIR=$(dirname "$(realpath "$0")")

source "$SCRIPT_DIR"/4_functions/hideicon.sh
source "$SCRIPT_DIR"/4_functions/hwinfo.sh
source "$SCRIPT_DIR"/4_functions/addgnomeshortcut.sh
source "$SCRIPT_DIR"/4_functions/pkgcheck.sh

# Create config directories
mkdir -p "$HOME"/.config/environment.d/
mkdir -p "$HOME"/.config/scripts/
mkdir -p "$HOME"/.local/share/applications/
mkdir -p "$HOME"/.local/bin

# Install local configs
for ENTRY in "$SCRIPT_DIR"/config/*; do
	if pkgexists "$(basename "$ENTRY")"; then
		cp -r "$ENTRY" "$HOME"/.config/
	fi
done

# Install scripts used by the system
if pkgexists gnome-session; then
	cp "$SCRIPT_DIR"/config/scripts/dark-mode.sh "$HOME"/.config/scripts/
	cp "$SCRIPT_DIR"/config/scripts/update-notifier.sh "$HOME"/.config/scripts/
	cp "$SCRIPT_DIR"/config/scripts/stayturnedon.sh "$HOME"/.config/scripts/
fi
if pkgexists sway; then
	cp "$SCRIPT_DIR"/config/scripts/rofi.sh "$HOME"/.config/scripts/
	cp "$SCRIPT_DIR"/config/scripts/dark-mode.sh "$HOME"/.config/scripts/
fi

# Create local bin directory
mkdir -p "$HOME"/.local/bin/

# Install files to home
shopt -s dotglob
for FILE in "$SCRIPT_DIR"/home/*; do
	cp -r "$FILE" "$HOME"/
done
shopt -u dotglob

# Hide some icons
hideicon avahi-discover.desktop
hideicon bssh.desktop
hideicon bvnc.desktop
hideicon cmake-gui.desktop
hideicon cups.desktop
hideicon mpv.desktop
hideicon lstopo.desktop
hideicon org.gnome.Extensions.desktop
hideicon qt6ct.desktop
hideicon qv4l2.desktop
hideicon qvidcap.desktop
hideicon rofi.desktop
hideicon rofi-theme-selector.desktop
hideicon steam.desktop
hideicon vim.desktop
hideicon electron.desktop
for ELECTRON in /usr/share/applications/electron*[0-9].desktop; do hideicon "$(basename "$ELECTRON")"; done

# JDK
hideicon java-java-openjdk.desktop
hideicon jconsole-java-openjdk.desktop
hideicon jshell-java-openjdk.desktop
for OPENJDK in /usr/share/applications/java-java*[0-9]-openjdk.desktop; do hideicon "$(basename "$OPENJDK")"; done
for JCONSOLE in /usr/share/applications/jconsole-java*[0-9]-openjdk.desktop; do hideicon "$(basename "$JCONSOLE")"; done
for JSHELL in /usr/share/applications/jshell-java*[0-9]-openjdk.desktop; do hideicon "$(basename "$JSHELL")"; done

# Qt tools
hideicon linguist.desktop
hideicon designer.desktop
hideicon assistant.desktop
hideicon qdbusviewer.desktop

# Hide some apps in Gnome
hideingnome org.pulseaudio.pavucontrol.desktop
hideingnome Alacritty.desktop
hideingnome nm-connection-editor.desktop
hideingnome system-config-printer.desktop
hideingnome blueman-manager.desktop

renameicon steam-native.desktop "Steam (Native)" Steam

# Exclude the directory of hidden icons from gnome search
if pkgexists gnome-shell; then
	touch "$HOME"/.local/share/applications/.trackerignore
fi

# Enable theming for qt applications with qt6ct
if pkgexists qt6ct; then
	echo "QT_QPA_PLATFORMTHEME=qt6ct" > "$HOME"/.config/environment.d/qt-theming.conf
fi

# Set envvars for different GPUS
if [[ $(vga) = intel ]]; then
	echo "VDPAU_DRIVER=va_gl" > "$HOME"/.config/environment.d/intel-fix.conf
fi
if [[ $(vga) = amd ]]; then
	echo "VDPAU_DRIVER=radeonsi" > "$HOME"/.config/environment.d/amd-fix.conf
fi
if [[ $(vga) = nvidia ]]; then
	echo "LIBVA_DRIVER_NAME=nvidia" > "$HOME"/.config/environment.d/nvidia-fix.conf
fi

# Gnome settings
if pkgexists gnome-session; then
	# Add update notifier
	mkdir -p "$HOME"/.config/autostart/
	echo "[Desktop Entry]
Name=Update Notifier
GenericName=Update Notifier
Comment=Notification about new packages popping up when starting GNOME
Exec=$HOME/.config/scripts/update-notifier.sh
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true" > "$HOME"/.config/autostart/update-notifier.desktop

	# Configure theme
	gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3-dark'
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
	gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'

	# Add some shortcuts
	addgnomeshortcut dark-mode-switch 'Dark mode switch' '<Super>d' "$HOME/.config/scripts/dark-mode.sh"
	addgnomeshortcut disable-screen-lock 'Disable screen lock' '<Super><Shift>l' "$HOME/.config/scripts/stayturnedon.sh"
	addgnomeshortcut mute 'Mute' '<Super>Period' 'wpctl set-mute @DEFAULT_SOURCE@ 1'
	addgnomeshortcut unmute 'Unmute' '<Super>Comma' 'wpctl set-mute @DEFAULT_SOURCE@ 0'

	# Add easily changeable custom shortcuts (Super + F1-F12)
	if [[ ! -e $HOME/.shortcuts ]]; then
		echo -e "#!/bin/bash\ncase \$1 in" > "$HOME"/.shortcuts
		for i in {1..12}; do
			echo -e "\t$i)\n\t\t\n\t;;" >> "$HOME"/.shortcuts
		done
		echo 'esac' >> "$HOME"/.shortcuts
		chmod +x "$HOME"/.shortcuts
	fi

	for i in {1..12}; do
		addgnomeshortcut "super_f$i" "Super + F$i" "<Super>F$i" "$HOME/.shortcuts $i"
	done

	# Set window controls
	gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
	gsettings set org.gnome.desktop.wm.preferences focus-mode click
	gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true
	gsettings set org.gnome.desktop.wm.preferences titlebar-uses-system-font true
	gsettings set org.gnome.mutter attach-modal-dialogs false
	gsettings set org.gnome.desktop.wm.keybindings switch-applications "['<Alt>Tab']"
	gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Super>Tab']"
	gsettings set org.gnome.shell.window-switcher app-icon-mode 'both'
	gsettings set org.gnome.shell.window-switcher current-workspace-only true
	gsettings set org.gnome.shell.app-switcher current-workspace-only false
	gsettings set org.gnome.desktop.wm.keybindings cycle-windows "['<Alt>Escape', '<Super>Escape']"
	gsettings set org.gnome.desktop.wm.keybindings cycle-windows-backward "['<Shift><Alt>Escape', '<Shift><Super>Escape']"

	# Add apps to dock
	gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Console.desktop', 'org.inkscape.Inkscape.desktop', 'dev.zed.Zed.desktop', 'org.gnome.TextEditor.desktop']"

	# Reset app order
	gsettings reset org.gnome.shell app-picker-layout

	# Set keyboard layout
	gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'us'), ('xkb', 'hu+102_qwerty_comma_nodead')]"

	# Enable tap-to-click
	gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true

	# Disable feedback sounds
	gsettings set org.gnome.desktop.sound input-feedback-sounds false
	gsettings set org.gnome.desktop.sound event-sounds false

	# Set fonts
	gsettings set org.gnome.desktop.interface font-hinting 'full'
	gsettings set org.gnome.desktop.interface font-antialiasing 'grayscale'
	gsettings set org.gnome.desktop.interface font-name 'Inter 11'
	gsettings set org.gnome.desktop.interface document-font-name 'Inter 11'
	gsettings set org.gnome.desktop.interface monospace-font-name 'Source Code Pro 11'

	# Do not automatically suspend on AC
	gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'

	# Some privacy
	gsettings set org.gnome.desktop.lockdown disable-lock-screen false
	gsettings set org.gnome.desktop.lockdown disable-show-password true
fi

# Gnome App settings
if pkgexists gnome-console; then
	gsettings set org.gnome.Console theme 'auto'
	gsettings set org.gnome.Console audible-bell false
	gsettings set org.gnome.Console visual-bell false
fi
if pkgexists gnome-text-editor; then
	gsettings set org.gnome.TextEditor indent-style 'tab'
	gsettings set org.gnome.TextEditor tab-width 'uint32 4'
	gsettings set org.gnome.TextEditor show-line-numbers true
fi
if pkgexists gnome-boxes; then
	gsettings set org.gnome.boxes first-run false
fi

# Sway settings
if pkgexists sway; then
	mkdir -p "$HOME"/.config/sway/config.d
	if [[ $(system_id_hash) = "$my_pc_id_hash" ]]; then
		echo 'output DP-1 mode 2560x1440@165.000Hz' > "$HOME"/.config/sway/config.d/monitor.conf
		echo 'for_window [class="steam_app*"] inhibit_idle focus' > "$HOME"/.config/sway/config.d/games-noidle.conf
	fi
	if is_laptop; then
		echo "bindsym XF86MonBrightnessDown exec xbacklight -dec 10
bindsym XF86MonBrightnessUp exec xbacklight -inc 10" > "$HOME"/.config/sway/config.d/backlight.conf
	fi
fi

# Libreoffice settings
if pkgexists libreoffice-fresh; then
	mkdir -p ~/.config/libreoffice/4/user/
	lo_version=$(libreoffice --version | awk 'NR==1 {print $2}' | cut -d. -f1-2)
	lo_build=$(libreoffice --version | awk 'NR==1 {print $3}')
	cat << EOF > "$HOME"/.config/libreoffice/4/user/registrymodifications.xcu
<?xml version="1.0" encoding="UTF-8"?>
<oor:items xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<item oor:path="/org.openoffice.Office.Common/Misc"><prop oor:name="FirstRun" oor:op="fuse"><value>false</value></prop></item>
<item oor:path="/org.openoffice.Office.Common/Misc"><prop oor:name="ShowTipOfTheDay" oor:op="fuse"><value>false</value></prop></item>
<item oor:path="/org.openoffice.Setup/Office"><prop oor:name="LastCompatibilityCheckID" oor:op="fuse"><value>$lo_build</value></prop></item>
<item oor:path="/org.openoffice.Setup/Office"><prop oor:name="ooSetupInstCompleted" oor:op="fuse"><value>true</value></prop></item>
<item oor:path="/org.openoffice.Office.Common/Misc"><prop oor:name="UseOpenCL" oor:op="fuse"><value>true</value></prop></item>
<item oor:path="/org.openoffice.Office.Common/Misc"><prop oor:name="Persona" oor:op="fuse"><value>no</value></prop></item>
<item oor:path="/org.openoffice.Office.Common/Misc"><prop oor:name="PersonaSettings" oor:op="fuse"><value></value></prop></item>
<item oor:path="/org.openoffice.Setup/Product"><prop oor:name="LastTimeDonateShown" oor:op="fuse"><value>10000000000</value></prop></item>
<item oor:path="/org.openoffice.Setup/Product"><prop oor:name="LastTimeGetInvolvedShown" oor:op="fuse"><value>10000000000</value></prop></item>
<item oor:path="/org.openoffice.Setup/Product"><prop oor:name="ooSetupLastVersion" oor:op="fuse"><value>$lo_version</value></prop></item>
<item oor:path="/org.openoffice.Office.Common/Misc"><prop oor:name="SymbolStyle" oor:op="fuse"><value>sifr_dark</value></prop></item>
</oor:items>
EOF
fi

# Set template files
if [[ -d $HOME/Sablonok ]]; then
	echo "" > "$HOME/Sablonok/Üres fájl"
	echo "#! /bin/bash" > "$HOME/Sablonok/Shell szkript"
fi

# Set background
if pkgexists gnome-session || pkgexists sway; then
	if ! [[ -f $HOME/.local/share/backgrounds/moss.jpg ]]; then
		mkdir -p "$HOME"/.local/share/backgrounds/
		cp "$SCRIPT_DIR"/wallpaper/moss.jpg "$HOME"/.local/share/backgrounds/
	fi
	if pkgexists gnome-session; then
		gsettings set org.gnome.desktop.background picture-uri "'file://""$HOME/.local/share/backgrounds/moss.jpg'"
		gsettings set org.gnome.desktop.background picture-uri-dark "'file://""$HOME/.local/share/backgrounds/moss.jpg'"
		gsettings set org.gnome.desktop.background primary-color '#232323'
	fi
fi

# Make this directory hidden
# I keep up to date this script and run it on my machines when I change my settings, but I don't need it to show un among my regular directories
if [[ "$SCRIPT_DIR" == "$HOME/arch-setup" ]]; then
	cd "$SCRIPT_DIR"/.. || exit
	mv arch-setup .arch-setup
	echo "Script directory moved to $HOME/.arch-setup"
fi
